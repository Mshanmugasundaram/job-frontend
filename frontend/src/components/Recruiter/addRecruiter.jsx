import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {Multiselect} from 'multiselect-react-dropdown'
// import Select from 'react-select'
import Chip from '@material-ui/core/Chip';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

class AddRecruiter extends Component {
    state = {
        name: '',
        description: '',
        email: '',
        address: '',
        type: [],
        companies: []
    }

    async componentWillMount() {
        let token = localStorage.getItem('token')
        await this.company()
    }

    company = async() => {
        let token = await localStorage.getItem('token')
        await axios.get(`http://localhost:7000/company/all`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            let data = response.data.data
            this.setState({companies: data})
            console.log(this.state.companies)
        }).catch(err => alert(err))
    }

    companyHandleChange = (e) => {
        console.log(e.target.value)
        this.setState({company: e.target.value})
    }
  
    addRecruiter = async() => {
        let token = await localStorage.getItem('token')
        await axios.post(`http://localhost:7000/recruiter/add`, {
            company: this.state.company
        }, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            alert("Added Successfully")
            localStorage.setItem('recruiter', true)
            this.props.history.push('/jobs')
        }).catch(err => alert(err))
    }
    render() {
        let skills = this.state.skills
        let token = localStorage.getItem('token')
        console.log(skills)
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
                <div style={{marginTop: '25px',fontSize: '13px'}}>NOTE: You're a recruiter from a company. Give your company details</div>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Company Details</h1>
            <Grid item>
            <FormControl variant='outlined' style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
                    <InputLabel id="demo-simple-select-label">Select Company</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={this.state.company}
                        variant='outlined'
                        required 
                        onChange={(e) => this.companyHandleChange(e)}
                    >
                        { this.state.companies.map((company) => {
                            return (<MenuItem style={{textTransform: 'capitalize'}} value={company._id}>{company.name}
                            </MenuItem>)
                        })
                    }
                </Select>
                </FormControl>
                </Grid>
            <Grid item>
            <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                    marignBotton: '40px',
                    backgroundColor:'#4caf50'
                }}
                variant='outlined'
                color='secondary'
                onClick = {() => this.addRecruiter()}
                >Register</Button>
            </Grid>
            </div>
        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(AddRecruiter);
// export default withStyles(useStyles, { withTheme: true })(Login);