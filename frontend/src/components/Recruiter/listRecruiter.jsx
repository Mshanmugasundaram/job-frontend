import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
import DescriptionIcon from '@material-ui/icons/Description';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import LocationOnIcon from '@material-ui/icons/LocationOn';


const theme = createMuiTheme({
    
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class ListRecruiter extends Component {
    state = {
        details: []
    }

    // request = async(token) => {
    //     let companyId = await sessionStorage.getItem('companyId')
    //     await axios.get(`http://localhost:7000/recruiter/request/${companyId}`,{
    //         headers: {
    //             Authorization: `JWT ${token}`
    //         }
    //     }).then(response => {
    //         let data = response.data.data
    //         this.setState({details: data})
    //     }).catch(err => alert(err))
    // }

    list = async(id) => {
        console.log(id)
        let token = await localStorage.getItem('token')
        console.log(token)
        axios.get(`http://localhost:7000/recruiter/list/${id}`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            let data = response.data.data
            console.log(data)
            this.setState({details: data})
            
            // alert("Approved successfully")
            // this.props.history.push('/request')
        }).catch(err => alert(err))
    }

    back = () => {
        this.props.history.push('/jobs')
    }

    async componentDidMount(){
        let token = await localStorage.getItem('token')
        let companyId = await sessionStorage.getItem('companyId')
        console.log(token)
        await this.list(companyId)
    }
    render () {
        let recruiters = this.state.details
        console.log(recruiters)
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                    textAlign: 'center',
                    marginTop: '30px',}}>
               <Typography variant='h4' style={{fontFamily: 'Helvetica', textTransform: 'capitalize', letterSpacing: 2, marginBottom: '30px',  borderRadius: '15px'}}>
                    Welcome <b>{localStorage.getItem('username')}..!!</b>
                    <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '340px',  borderRadius: '15px'}} variant='outlined' onClick = {() => this.back()}>Go Back</Button>
                    {/* {localStorage.getItem('role') == 'recruiterseeker'  ? <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.applied()}>Applied recruiters</Button> : <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.postrecruiters()}>Post recruiters</Button>}
                    {localStorage.getItem('role') == 'recruiter' ?<Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.applied()}>Posted recruiters</Button> : ''} */}
                    
                    </Typography>
                <hr/>
                <h1 style={{
                    fontWeight: 400,
                    fontFamily: 'sans-serif',
                    color: '#4caf50',
                    // textTransform: 'uppercase',
                    letterSpacing: '2px',
                    display: 'inline-block'
                }}>Recruiters</h1>
                </div>
                <Grid item>
                    {this.state.details.length == 0 ? <h3 style={{fontFamily: 'sans-serif',textTransform: 'capitalize', textAlign: 'center'}}>
                        No request available
                    </h3> : this.state.details.map(recruiter => {
                        return (
                            <div style={{ margin: '20px 180px', backgroundColor: '#f1f1f1', padding: "10px 15px", borderRadius: '10px'}}>
                            <h2 style={{fontFamily: 'sans-serif',textTransform: 'capitalize'}}>User Name - {recruiter.username}</h2>
                            <DescriptionIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><strong>Name - </strong> {recruiter.name}</h4><br/>
                            <AccountCircleIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px', display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400}}><b>Experience - </b> {recruiter.experience}</h4><br/>
                            <AttachMoneyIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400}}><b>Gender - </b> {recruiter.gender}</h4><br/>
                            <LocationOnIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><b>Qualification - </b>{recruiter.qualification}</h4><br/>
                            {/* <Button style={{backgroundColor: '#f1f1f1', color: '#4caf50', marginRight: '40px',padding: '5px 15px', marginBottom: "10px", borderRadius: '5px', border: 'solid .5px'}} onClick={() => this.approve(recruiter.id)} fullWidth>Approve</Button> */}
                            </div>
                        )
                    })}
                </Grid>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(ListRecruiter)