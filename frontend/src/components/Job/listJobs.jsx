import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
import DescriptionIcon from '@material-ui/icons/Description';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import WorkIcon from '@material-ui/icons/Work';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ReorderIcon from '@material-ui/icons/Reorder';


const theme = createMuiTheme({
    
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class ListJobs extends Component {
    state = {
        jobs:[]
    }

    listJobs = async(token) => {
        await axios.get(`http://localhost:7000/job`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        })
            .then(response => {
                console.log(response.data.data)
                this.setState({jobs: response.data.data})
                console.log(this.state.jobs)
            })
            .catch(err => alert(err))
    }

    viewJob = async(id) => {
        await sessionStorage.setItem('jobId', id)
        this.props.history.push({
            pathname:'/view/job'
        })
    }

    // apply = (jobId) => {
    //     console.log(jobId)
    //     this.props.history.push({
    //         pathname: '/apply',
    //         jobId: jobId
    //     })
    // }

    postJobs = () => {
        this.props.history.push('/post/job')
    }

    applied = () => {
        this.props.history.push('/applied')
    }

    profileDetails = () => {
        this.props.history.push('/profile')
    }

    profile = () => {
        this.props.history.push('/add/profile')
    }

    postedJobs = () => {
        this.props.history.push('/posted/jobs')
    }

    company = () => {
        console.log('hello')
        this.props.history.push('/company')
    }

    recruiter = () => {
        this.props.history.push('/add/recruiter')
    }

    request = () => {
        this.props.history.push('/request')
    }

    recruiterList = () => {
        this.props.history.push('/list')
    }

    async componentWillMount(){
        let token = await localStorage.getItem('token')
        console.log(token)
        await this.listJobs(token)
        // {localStorage.getItem('company') != 'true' || null ? '' : <div style={{marginTop: '25px',fontSize: '13px'}}>NOTE: You role is registered as a company owner so you have to enter your company details to proceed further..!!</div>}
        let isCompany  = await localStorage.getItem('company')
        console.log(isCompany)
        if(isCompany == false) {
            this.company()
        }
        let isProfile  = await localStorage.getItem('profile')
        console.log(isProfile)
        if( isProfile == false || isProfile == 'false') {
            this.profile()
        }
        let isRecruiter = await localStorage.getItem('recruiter') 
        console.log(isRecruiter)
        if(isRecruiter == 'false') {
            this.recruiter()
        }
    }
    render () {
        console.log(this.state.jobs)
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header/>
                </Grid>
                <div style={{
                    textAlign: 'center',
                    marginTop: '30px',}}>
                <Typography variant='h4' style={{fontFamily: 'Helvetica', textTransform: 'capitalize', letterSpacing: 2, marginBottom: '30px',  borderRadius: '15px'}}>
                    Welcome <b>{localStorage.getItem('username')}..!!</b>

                    {localStorage.getItem('role') != 'owner'? <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '340px',  borderRadius: '15px'}} variant='outlined' onClick = {() => this.profileDetails()}>Profile</Button> :''}
                    {localStorage.getItem('role') == 'jobseeker'  ? <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.applied()}>Applied Jobs</Button> : ''}
                    {localStorage.getItem('role') == 'recruiter' ? <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.postJobs()}>Post Jobs</Button>: ''}
                    {localStorage.getItem('role') == 'recruiter' ?<Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.postedJobs()}>Posted Jobs</Button> : ''}
                    
                    {localStorage.getItem('role') == 'owner' ?<Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '300px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.company()}><LocationCityIcon style={{marginRight: '5px'}}/> Company</Button> : ''}
                    {localStorage.getItem('role') == 'owner' ?<Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '20px',borderRadius: '15px'}} variant='outlined' onClick={()=> this.request()}><AddCircleOutlineIcon style={{marginRight: '5px'}}/> Request</Button> : ''}
                    {localStorage.getItem('role') == 'owner' ?<Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '20px',borderRadius: '15px'}} variant='outlined' onClick={()=> this.recruiterList()}><ReorderIcon style={{marginRight: '5px'}}/> Recruiter</Button> : ''}
                    {localStorage.getItem('profile') == 'true' ? '' : <div style={{marginTop: '25px',fontSize: '13px'}}>NOTE: You should create a profile initially to use our full feature else some of our feature won't work..!!</div>}
                    </Typography>
                <hr/>
                <h1 style={{
                    fontWeight: 400,
                    fontFamily: 'sans-serif',
                    color: '#4caf50',
                    // textTransform: 'uppercase',
                    letterSpacing: '2px',
                    display: 'inline-block'
                }}>Jobs</h1>
                </div>
                <Grid item>
                    {this.state.jobs.map(job => {
                        return (
                            <div style={{ margin: '20px 180px', backgroundColor: '#f1f1f1', padding: "10px 15px", borderRadius: '10px'}}>
                            <h2 style={{fontFamily: 'sans-serif',textTransform: 'capitalize'}}>Job - {job.job}</h2>
                            <LocationCityIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><strong>Company - </strong> {job.company}</h4><br/>
                            <DescriptionIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><strong>Description - </strong> {job.description}</h4><br/>
                            <AccountCircleIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px', display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400}}><b>Experience - </b> {job.experience}</h4><br/>
                            <AttachMoneyIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400}}><b>Salary - </b> {job.salary}</h4><br/>
                            <LocationOnIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><b>Location - </b>{job.location.toString()}</h4><br/>
                            <Button style={{backgroundColor: '#f1f1f1', color: '#4caf50', marginRight: '40px',padding: '5px 15px', marginBottom: "10px", borderRadius: '5px', border: 'solid .5px'}} onClick={() => this.viewJob(job.id)}  fullWidth>View Job</Button>
                            {/* <Button style={{backgroundColor: '#4caf50', color: '#fffff', marginRight: '40px', padding: '5px 15px',}} onClick={()=>this.apply(job.id)}>Apply</Button> */}
                            </div>
                        )
                    })}
                </Grid>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(ListJobs)