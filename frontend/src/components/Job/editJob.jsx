import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

class EditJob extends Component {
    state = {
        skills: [],
        skillSet: [],
        job: ''

    }

    postJob = async(id) => {
        console.log(id)
        let token = await localStorage.getItem('token')
        await axios.put(`http://localhost:7000/job/edit/${id}`,{
            name: this.state.name == undefined ? this.state.jobDetails.job : this.state.name,
            description: this.state.description == undefined ? this.state.jobDetails.description : this.state.description ,
            location: this.state.location == undefined ? this.state.jobDetails.description : this.state.location,
            experience: this.state.experience== undefined ? this.state.jobDetails.experience : this.state.experience,
            salary: this.state.salary== undefined ? this.state.jobDetails.salary : this.state.salary,
            skills: this.state.skillSet.length == 0 ? this.state.jobDetails.skills : this.state.skillSet,
            industry: this.state.industry == undefined ? this.state.jobDetails.industry : this.state.industry,
        }, {
            headers: {
                Authorization: `JWT ${token}`,
            }
        }).then(response => {
            console.log(response)
            alert("Job edited successfully")
            this.props.history.push('/jobs')
        }).catch(err => alert(err))
    }

    job = async(token) => {
        console.log(token)
        let jobId = await sessionStorage.getItem('jobId')
        await axios.get(`http://localhost:7000/job/${jobId}`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        })
            .then(response => {
                console.log(response.data.data)
                let data = response.data.data
                this.setState({jobDetails: data})
                console.log(this.state.jobDetails.skills)
            })
            .catch(err => {
                alert(err)
            })
    }

    listSkills = (token) => {
        axios.get(`http://localhost:7000/skill/list`)
            .then(response => {
                let data = response.data.data
                console.log(data)
                this.setState({skills: data})
                console.log(this.state.skills)
                // {this.state.skills.map(skill => {
                //     console.log(skill.name)
                // })}
            }).catch(err => alert(err))
    }

    async componentDidMount() {
        let token = localStorage.getItem('token')
        await this.listSkills(token)
        await this.job(token)
    }

    skillSet = (e) => {
        console.log(this.state.skillSet)
        // console.log(e.target.value.toString())
        // let skills = [...this.state.skillSet]
        // console.log(e.target.value)
        // console.log(skills)
        // skills.push(e.target.value)
        // console.log(skills)
        this.setState({skillSet: e.target.value})
    }



    render() {
        let skills = this.state.skills
        let jobDetails = this.state.jobDetails
        // console.log()
        let token = localStorage.getItem('token')
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Edit Job</h1>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Job Name'
                defaultValue = { jobDetails != undefined ? jobDetails.job : '' }
                autoComplete
                onChange={(e) => this.setState({ name: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Job Description'
                defaultValue = { jobDetails != undefined ? jobDetails.description : '' }
                autoComplete
                onChange={(e) => this.setState({ description: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Location'
                defaultValue = { jobDetails != undefined ? jobDetails.location : '' }
                autoComplete
                onChange={(e) => this.setState({ location: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Salary'
                defaultValue = { jobDetails != undefined ? jobDetails.salary : '' }
                autoComplete
                onChange={(e) => this.setState({ salary: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Experience'
                defaultValue = { jobDetails != undefined ? jobDetails.experience : '' }
                autoComplete
                onChange={(e) => this.setState({ experience: e.target.value})}
                />
            </Grid>
            <Grid item>
            <FormControl style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
                        <InputLabel id="demo-simple-select-label">Select Skill</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.skillSet}
                            variant='outlined'
                            defaultValue = { jobDetails != undefined ? jobDetails.skills : '' }
                            onChange={(e) => this.skillSet(e)}
                            multiple
                            renderValue={(selected) => (
                                // console.log(selected),
                                selected.join(', ')
                              )}
                        >
                            { this.state.skills.map((skill) => {
                                return (<MenuItem value={skill.name}>
                                    <Checkbox checked={this.state.skillSet.indexOf(skill.name) > -1} />
                                    <ListItemText primary={skill.name} />
                                </MenuItem>)
                            })
                    }
                </Select>
                </FormControl>
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Industry'
                defaultValue = { jobDetails != undefined ? jobDetails.industry : '' }
                autoComplete
                onChange={(e) => this.setState({ industry: e.target.value})}
                />
            </Grid>
            <Grid>
            <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                    marignBotton: '40px',
                    backgroundColor:'#4caf50'
                }}
                variant='outlined'
                color='secondary'
                onClick = {() => this.postJob(jobDetails.id)}
                >Edit</Button>
            </Grid>
            </div>

        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(EditJob);
// export default withStyles(useStyles, { withTheme: true })(Login);