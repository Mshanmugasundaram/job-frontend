import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

class PostJobs extends Component {
    state = {
        skills: [],
        skillSet: []

    }

    postJob = async(token) => {
        // let token = await localStorage.getItem('token')
        console.log(token)
        let input = this.state
        await axios.post(`http://localhost:7000/job/add`,{
                name: input.name,
                description: input.description,
                location: input.location,
                salary: input.salary,
                experience: input.experience,
                skills: input.skillSet,
                industry: input.industry
        }, {
            headers: {
                Authorization: `JWT ${token}`,
            }
        }).then(response => {
            console.log(response)
            alert("Job added successfully")
            this.props.history.push('/jobs')
        }).catch(err => alert(err))
    }

    listSkills = (token) => {
        axios.get(`http://localhost:7000/skill/list`)
            .then(response => {
                let data = response.data.data
                console.log(data)
                this.setState({skills: data})
                console.log(this.state.skills)
                // {this.state.skills.map(skill => {
                //     console.log(skill.name)
                // })}
            }).catch(err => alert(err))
    }

    async componentWillMount() {
        let token = localStorage.getItem('token')
        await this.listSkills(token)
    }

    skillSet = (e) => {
        console.log(this.state.skillSet)
        // console.log(e.target.value.toString())
        // let skills = [...this.state.skillSet]
        // console.log(e.target.value)
        // console.log(skills)
        // skills.push(e.target.value)
        // console.log(skills)
        this.setState({skillSet: e.target.value})
    }



    render() {
        let skills = this.state.skills
        let token = localStorage.getItem('token')
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Add Job</h1>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Job Name'
                autoComplete
                onChange={(e) => this.setState({ name: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Job Description'
                autoComplete
                onChange={(e) => this.setState({ description: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Location'
                autoComplete
                onChange={(e) => this.setState({ location: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Salary'
                autoComplete
                onChange={(e) => this.setState({ salary: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Experience'
                autoComplete
                onChange={(e) => this.setState({ experience: e.target.value})}
                />
            </Grid>
            <Grid item>
            <FormControl style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
                        <InputLabel id="demo-simple-select-label">Select Skill</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.skillSet}
                            variant='outlined'
                            onChange={(e) => this.skillSet(e)}
                            multiple
                            renderValue={(selected) => (
                                // console.log(selected),
                                selected.join(', ')
                              )}
                        >
                            { this.state.skills.map((skill) => {
                                console.log(skill)
                                return (<MenuItem value={skill.name}>
                                    <Checkbox checked={this.state.skillSet.indexOf(skill.name) > -1} />
                                    <ListItemText primary={skill.name} />
                                </MenuItem>)
                            })
                    }
                </Select>
                </FormControl>
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Industry'
                autoComplete
                onChange={(e) => this.setState({ industry: e.target.value})}
                />
            </Grid>
            <Grid>
            <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                    marignBotton: '40px',
                    backgroundColor:'#4caf50'
                }}
                variant='outlined'
                color='secondary'
                onClick = {() => this.postJob(token)}
                >Post</Button>
            </Grid>
            </div>

        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(PostJobs);
// export default withStyles(useStyles, { withTheme: true })(Login);