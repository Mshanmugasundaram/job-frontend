import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
import LocationCityIcon from '@material-ui/icons/LocationCity';
import DescriptionIcon from '@material-ui/icons/Description';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople'
import WorkIcon from '@material-ui/icons/Work';


const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class ViewJob extends Component {
    state = {
        jobDetails: [],
        skills: []
    }

    viewJob = async(token) => {
        console.log(token)
        let jobId = await sessionStorage.getItem('jobId')
        await axios.get(`http://localhost:7000/job/${jobId}`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        })
            .then(response => {
                console.log(response.data.data)
                let data = response.data.data
                this.setState({jobDetails: data})
                console.log(this.state.jobDetails)
            })
            .catch(err => {
                alert(err)
            })

    }

    apply = (jobId) => {
        this.props.history.push({
            pathname: '/apply',
            jobId: jobId
        })
    }

    goBack = () => {
        this.props.history.push('/jobs')
    }

    async componentWillMount(){
        let token = await localStorage.getItem('token')
        let jobId = await sessionStorage.getItem('jobId')
        console.log(jobId)
        await this.viewJob(token)
    }
    render () {
        const job = this.state.jobDetails
        sessionStorage.setItem('jobName', job.job)
        sessionStorage.setItem('company', job.company)
        console.log(job.skills)
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                textAlign: 'center',
                // marginTop: 'px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Job Details</h1>
            </div>
            <Grid item>
                <div style={{margin: '20px 80px', backgroundColor: '#f1f1f1', padding: "10px 15px", borderRadius: '10px', justifyContent:'center'}}>
                    {/* <WorkIcon style={{display: 'inline'}}/> */}
                    <h2 style={{ fontFamily: 'sans-serif', textTransform: 'capitalize', textAlign: 'center'}}>{job.job}</h2> <br></br>
                    <DescriptionIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Description - </b> {job.description}</h4><br/>
                    <LocationCityIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Company - </b> {job.company}</h4><br/>
                    <EmojiPeopleIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}> <b>Recruiter - </b> {job.recruiter}</h4><br/>
                    <AttachMoneyIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, display: 'inline-block'}}> <b>Salary - </b> {job.salary}</h4><br/>
                    <AccessibilityIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Skills - </b> {job.skills != undefined ? job.skills.toString() : ''}</h4><br/>
                    <AccountCircleIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, display: 'inline-block'}}> <b>Experience - </b> {job.experience}</h4><br/>
                    <LocationOnIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize',display: 'inline-block'}}> <b>Location - </b> {job.location != undefined ? job.location.toString() : ''}</h4><br/>
                    <LocationCityIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, display: 'inline-block'}}> <b>Industry - </b> {job.industry}</h4><br/>

                    {localStorage.getItem('role') == 'jobseeker' ?<Button style={{backgroundColor:'#f1f1f1', color: '#4caf50', border: 'solid .5px'}} fullWidth onClick={() => this.apply()}>Apply</Button> : <Button style={{backgroundColor:'#f1f1f1', color: '#4caf50', border: 'solid .5px'}} fullWidth onClick={() => this.goBack()}>Go Back</Button>}
                </div>
            </Grid>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(ViewJob)