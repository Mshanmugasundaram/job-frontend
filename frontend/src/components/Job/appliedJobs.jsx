import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'


const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class AppliedJobs extends Component {
    state = {
        jobs:[]
    }

    appliedJobs = (token) => {
        axios.get(`http://localhost:7000/job/applied`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            let data = response.data.data
            this.setState({jobs: data})
            console.log(this.state.jobs)
        })
            .catch(err => alert(err))

    }

    goBack = () => {
        this.props.history.push('/jobs')
    }
    async componentWillMount(){
        let token = await localStorage.getItem('token')
        console.log(token)
        this.appliedJobs(token)
    }
    render () {
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                    textAlign: 'center',
                    marginTop: '30px',}}>
                {/* <Typography variant='h4' style={{fontFamily: 'poppins', textTransform: 'capitalize', letterSpacing: 2, marginBottom: '30px'}}>
                    Welcome {localStorage.getItem('username')}..!! */}
                <h1 style={{
                    fontWeight: 400,
                    fontFamily: 'sans-serif',
                    color: '#4caf50',
                    // textTransform: 'uppercase',
                    letterSpacing: '2px',
                    display: 'inline-block'
                }}>Applied Jobs</h1>
                    <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '340px'}} variant='outlined' onClick={() => this.goBack()}>Go Back</Button>

                <hr/>
                </div>
                <Grid item>
                    {this.state.jobs ? this.state.jobs.map(job => {
                        console.log(job)
                        return (
                            <div style={{margin: '20px 80px', backgroundColor: '#f1f1f1', padding: "10px 15px", borderRadius: '10px'}}>
                            <h3 style={{fontFamily: 'sans-serif', textTransform: 'capitalize'}}>Job: {job.name}</h3>
                            <h4 style={{ fontFamily:'sans-serif', fontWeight: 400}}>Description: {job.description}</h4>
                            <h4 style={{ fontFamily:'sans-serif', fontWeight: 400}}>Experience: {job.experience}</h4>
                            <h4 style={{ fontFamily:'sans-serif', fontWeight: 400}}>Salary: {job.salary}</h4>
                            <h4 style={{ fontFamily:'sans-serif', fontWeight: 400}}>Location: {job.location}</h4>
                            {/* <Button style={{backgroundColor: '#4caf50', color: '#fffff', marginRight: '40px', padding: '5px 15px',}} onClick={() => this.viewJob(job.id)}>View Job</Button> */}
                            {/* <Button style={{backgroundColor: '#4caf50', color: '#fffff', marginRight: '40px', padding: '5px 15px',}} onClick={()=>this.apply(job.id)}>Apply</Button> */}
                            </div>
                        )
                    }): 
                    <div style={{
                        textAlign: 'center',
                        marginTop: '30px',}}>
                            <h2 style={{fontFamily: 'sans-serif', textTransform: 'capitalize', fontWeight: 400}}> No Jobs available</h2></div>}
                </Grid>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(AppliedJobs)