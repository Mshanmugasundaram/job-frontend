import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
import DescriptionIcon from '@material-ui/icons/Description';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import LocationOnIcon from '@material-ui/icons/LocationOn';


const theme = createMuiTheme({
    
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class PostedJobs extends Component {
    state = {
        jobs:[]
    }

    listJobs = (token) => {
        axios.get(`http://localhost:7000/recruiter/posted/jobs`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        })
            .then(response => {
                this.setState({jobs: response.data.data})
                console.log(this.state.jobs)
            })
            .catch(err => alert(err))
    }

    viewJob = async(id) => {
        console.log(id)
        await sessionStorage.setItem('jobId', id)
        this.props.history.push({
            pathname:'/view/job'
        })
    }

    editJob = () => {
        this.props.history.push('/edit/job')
    }

    // apply = (jobId) => {
    //     console.log(jobId)
    //     this.props.history.push({
    //         pathname: '/apply',
    //         jobId: jobId
    //     })
    // }

    postJobs = () => {
        this.props.history.push('/post/job')
    }

    applied = () => {
        this.props.history.push('/applied')
    }

    viewJob = async(id) => {
        await sessionStorage.setItem('jobId', id)
        this.props.history.push({
            pathname:'/view/job'
        })
    }

    async componentWillMount(){
        let token = await localStorage.getItem('token')
        console.log(token)
        this.listJobs(token)
    }
    render () {
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                    textAlign: 'center',
                    marginTop: '30px',}}>
                <Typography variant='h4' style={{fontFamily: 'poppins', textTransform: 'capitalize', letterSpacing: 2, marginBottom: '30px',  borderRadius: '15px'}}>
                    Welcome {localStorage.getItem('username')}..!!
                    <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '340px',  borderRadius: '15px'}} variant='outlined' onClick = {() => this.profile()}>Go Back</Button>
                    {/* {localStorage.getItem('role') == 'jobseeker'  ? <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.applied()}>Applied Jobs</Button> : <Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.postJobs()}>Post Jobs</Button>}
                    {localStorage.getItem('role') == 'recruiter' ?<Button style={{color:'white', backgroundColor:'#4caf50', marginLeft: '40px', borderRadius: '15px'}} variant='outlined' onClick={()=> this.applied()}>Posted Jobs</Button> : ''} */}
                    
                    </Typography>
                <hr/>
                <h1 style={{
                    fontWeight: 400,
                    fontFamily: 'sans-serif',
                    color: '#4caf50',
                    // textTransform: 'uppercase',
                    letterSpacing: '2px',
                    display: 'inline-block'
                }}>Posted Jobs</h1>
                </div>
                <Grid item>
                    {this.state.jobs.map(job => {
                        return (
                            <div style={{ margin: '20px 180px', backgroundColor: '#f1f1f1', padding: "10px 15px", borderRadius: '10px'}}>
                            <h2 style={{fontFamily: 'sans-serif',textTransform: 'capitalize'}}>Job - {job.name}</h2>
                            <DescriptionIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><strong>Description - </strong> {job.description}</h4><br/>
                            <AccountCircleIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px', display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400}}><b>Experience - </b> {job.experience}</h4><br/>
                            <AttachMoneyIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400}}><b>Salary - </b> {job.salary}</h4><br/>
                            <LocationOnIcon style={{display: 'inline'}}/>
                            <h4 style={{ marginLeft: '10px',display: 'inline-block', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><b>Location - </b>{job.location.toString()}</h4><br/>
                            <Button style={{backgroundColor: '#f1f1f1', color: '#4caf50', marginRight: '40px',padding: '5px 15px', marginBottom: "10px", borderRadius: '5px', border: 'solid .5px'}} onClick={() => this.editJob(job._id)} fullWidth>Edit Job</Button>
                            <Button style={{backgroundColor: '#f1f1f1', color: '#4caf50', marginRight: '40px', padding: '5px 15px', marginBottom: "10px", borderRadius: '5px', border: 'solid .5px'}} onClick = {() => this.viewJob(job._id)}fullWidth>View Job</Button>
                            </div>
                        )
                    })}
                </Grid>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(PostedJobs)