import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'


const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class ApplyJob extends Component {
    state = {
        jobDetails: []
    }


    apply = async() => {
        let id = await localStorage.getItem('id')
        let jobId = await sessionStorage.getItem('jobId')
        console.log(jobId)
        axios.put(`http://localhost:7000/job/apply/${jobId}`, {
            userId: id
        }).then(response => {
            console.log(response)
            this.props.history.push('/applied')
        }).catch(err => alert(err))
    }

    cancel = () => {
        this.props.history.push('/jobs')
    }

    async componentDidMount(){

    }
    render () {
        let job = sessionStorage.getItem('jobName')
        let company = sessionStorage.getItem('company')
        let userId = localStorage.getItem('id')
        let jobId = sessionStorage.getItem('jobId')
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
                <Grid item>
                    <div style={{margin: '40px 500px', backgroundColor: '#f1f1f1', padding: "10px 15px 20px 15px", borderRadius: '10px'}}>
                        <h2 style={{ fontFamily:'sans-serif', fontWeight: 400}}><b>Do you want to apply for the job..!!</b></h2>
                        <h3 style={{ fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><b>Job - </b> {job}</h3>
                        <h3 style={{ fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize'}}><b>Company - </b> {company}</h3>
                        <Button style={{backgroundColor:'#4caf50', color: '#ffffff', marginRight: '30px'}} onClick={() => this.apply()}>Yes</Button>
                        <Button style={{backgroundColor:'#4caf50', color: '#ffffff'}} onClick={() => this.cancel()}>No</Button>
                    </div>
                </Grid>
                </div>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(ApplyJob)