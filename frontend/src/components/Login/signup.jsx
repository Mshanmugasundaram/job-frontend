import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';


const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

class Signup extends Component {
    state = {
        email: '',
        password:'',
        username: '',
        role: ''
    }


    signup = () => {
        console.log(this.state)
        let input = this.state
        axios.post(`http://localhost:7000/user/signup`, {
            email: input.email,
            username: input.username,
            password: input.password,
            role: input.role
        }).then(response => {
            console.log(response)
            this.props.history.push('/')
        }).catch(err => alert(err))
    }

    // forgetPassword = () => {
    //     this.props.history.push("/forget/password")
    // }

    async componentDidMount() {
        // if(localStorage.getItem('token')) {
        //     this.props.history.push('/movies')
        // }
        console.log('entered')
    }

    render() {
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                {/* <Grid item>
                    <section style={{width: "100%", height: "800px", backgroundRepeat: 'no-repeat',backgroundImage: `url(${friends})`}}></section>
                </Grid> */}
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Login</h1>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Email'
                autoComplete
                onChange={(e) => this.setState({ email: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '25px',
                    width: '25%',
                }}
                variant='outlined'
                type='password'
                label='Password'
                onChange={(e) => this.setState({ password: e.target.value})}
                />
            </Grid>
            <Grid item>

            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '25px',
                    width: '25%',
                }}
                variant='outlined'
                label='Username'
                onChange={(e) => this.setState({ username: e.target.value})}
                />
            </Grid>
            <Grid item>
            <FormControl variant="outlined">
            <InputLabel htmlFor="outlined-age-native-simple">Role</InputLabel>
                <Select
                    value={this.state.role}
                    onChange={(e) => this.setState({role: e.target.value})}
                    style={{width: '390px',  marginBottom: '20px'}}
                    >
                    {/* <MenuItem aria-label="None" value="" /> */}
                    <MenuItem value={"jobseeker"}>Jobseeker</MenuItem>
                    <MenuItem value={"recruiter"}>Recruiter</MenuItem>
                    </Select>
                </FormControl>
            </Grid>

            <Grid item>
                <Button style={{
                    marginTop: '25px',
                    marginLeft: '20px',
                    backgroundColor:'#4caf50'
                }} variant='outlined'
                color='secondary'
                onClick = {() => this.signup()}
                >Signup</Button>
            </Grid>
            </div>
        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(Signup);
// export default withStyles(useStyles, { withTheme: true })(Login);