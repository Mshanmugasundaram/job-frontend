import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'


const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

class Login extends Component {
    state = {
        email: '',
        password:''
    }

    login = () => {
        let data = this.state
        axios.post('http://localhost:7000/user', {
            email: data.email, 
            password: data.password
        }).then((response) => {
            let data = response.data;
            console.log(data)
            localStorage.setItem('token', data.token);
            localStorage.setItem('role', data.role);
            localStorage.setItem('id', data._id);
            localStorage.setItem('username', data.username)
            localStorage.setItem('role', data.role);
            localStorage.setItem('email', data.email);
            localStorage.setItem('profile', data.profile);
            localStorage.setItem('company', data.company);
            localStorage.setItem('recruiter', data.recruiter);
            if(data) {
                this.props.history.push({
                    pathname: '/jobs',
                    user: data
                })
            }
        }).catch(err => {
            alert('Invalid Credentials..')
        })
    }

    signup = () => {
        this.props.history.push('/signup')
    }

    // forgetPassword = () => {
    //     this.props.history.push("/forget/password")
    // }

    render() {
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                // color: '#4caf50',
                color: 'green',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
                padding: '10px 10px'
            }}>Login</h1>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Email'
                autoComplete
                onChange={(e) => this.setState({ email: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '25px',
                    width: '25%',
                }}
                variant='outlined'
                type='password'
                label='Password'
                onChange={(e) => this.setState({ password: e.target.value})}
                />
            </Grid>
            <Grid item>
                <a href='#' style={{color: 'green'}}>Forget Password..!</a>
            </Grid>
            <Grid item>
                <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                    backgroundColor:'#4caf50'
                }}
                variant='outlined'
                color='secondary'
                onClick = {() => this.login()}
                >Login</Button>
                <Button style={{
                    marginTop: '25px',
                    marginLeft: '20px',
                    backgroundColor:'#4caf50'
                }} variant='outlined'
                color='secondary'
                onClick = {() => this.signup()}
                >Signup</Button>
            </Grid>
            </div>
        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(Login);
// export default withStyles(useStyles, { withTheme: true })(Login);