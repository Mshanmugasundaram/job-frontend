import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {Multiselect} from 'multiselect-react-dropdown'
// import Select from 'react-select'
import Chip from '@material-ui/core/Chip';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

class AddCompany extends Component {
    state = {
        name: '',
        description: '',
        email: '',
        address: '',
        type: []
    }





    async componentWillMount() {
        let token = localStorage.getItem('token')

    }

    company = async() => {
        let token = await localStorage.getItem('token')
        let input = this.state
        axios.post(`http://localhost:7000/company/add`, {
            name: input.name,
            description: input.description,
            email: input.email,
            address: input.address,
            type: input.type,
        }, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            alert("company registered successfully")
            localStorage.setItem('company', true)
            this.props.history.push('/jobs')
            
        }).catch(err => alert(err))
    }

  
    render() {
        let skills = this.state.skills
        let token = localStorage.getItem('token')
        console.log(skills)
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
                <div style={{marginTop: '25px',fontSize: '13px'}}>NOTE: You're role is a company owner so, you have to enter your company details to proceed further..!!</div>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Company Details</h1>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Email'
                autoComplete
                onChange={(e) => this.setState({ email: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Name'
                autoComplete
                onChange={(e) => this.setState({ name: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Description'
                autoComplete
                onChange={(e) => this.setState({ description: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Address'
                autoComplete
                onChange={(e) => this.setState({ address: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Type'
                autoComplete
                onChange={(e) => this.setState({ type: e.target.value})}
                />
            </Grid>
            <Grid item>
            <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                    marignBotton: '40px',
                    backgroundColor:'#4caf50'
                }}
                variant='outlined'
                color='secondary'
                onClick = {() => this.company()}
                >Register</Button>
            </Grid>
            </div>

        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(AddCompany);
// export default withStyles(useStyles, { withTheme: true })(Login);