import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
import LocationCityIcon from '@material-ui/icons/LocationCity';
import DescriptionIcon from '@material-ui/icons/Description';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople'
import WorkIcon from '@material-ui/icons/Work';
import MailIcon from '@material-ui/icons/Mail';


const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class CompanyDetails extends Component {
    state = {
        jobDetails: [],
        skills: []
    }

    companyDetails = async(token) => {
        await axios.get(`http://localhost:7000/company`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            let data = response.data.data
            console.log(data)
            sessionStorage.setItem('companyId', data._id)
            this.setState({'name': data.name})
            this.setState({'description': data.description})
            this.setState({'email': data.email})
            this.setState({'address': data.address})
            this.setState({'type': data.type})
            console.log(this.state.details)
        })
    }

    goBack = () => {
        this.props.history.push('/jobs')
    }

    edit = () => {
        this.props.history.push('/edit/company')
    }

    async componentDidMount(){
        let token = await localStorage.getItem('token')
        let jobId = await sessionStorage.getItem('jobId')
        console.log(jobId)
        await this.companyDetails(token)
    }
    render () {
        console.log(sessionStorage.getItem('companyId'))
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                textAlign: 'center',
                // marginTop: 'px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Company Details</h1>
            </div>
            <Grid item>
                <div style={{margin: '20px 80px', backgroundColor: '#f1f1f1', padding: "10px 15px", borderRadius: '10px', justifyContent:'center'}}>
                    <WorkIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Company - </b> {this.state.name }</h4><br/>
                    <DescriptionIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Description - </b> {this.state.description}</h4><br/>
                    <MailIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}> <b>Email - </b> {this.state.email }</h4><br/>
                    <AttachMoneyIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, display: 'inline-block'}}> <b>Type - </b> {this.state.type != undefined ? this.state.type.toString() : ''}</h4><br/>
                    <LocationOnIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize',display: 'inline-block'}}> <b>Location - </b> {this.state.address}</h4><br/>
                    <Button style={{backgroundColor:'#f1f1f1', color: '#4caf50', border: 'solid .5px', marginBottom: '20px'}} fullWidth onClick={() => this.edit()}>Edit</Button>
                    <Button style={{backgroundColor:'#f1f1f1', color: '#4caf50', border: 'solid .5px'}} fullWidth onClick={() => this.goBack()}>Go Back</Button>
                </div>
            </Grid>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(CompanyDetails)