import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {Multiselect} from 'multiselect-react-dropdown'
// import Select from 'react-select'
import Chip from '@material-ui/core/Chip';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

class EditCompany extends Component {
    state = {
        name: '',
        description: '',
        email: '',
        address: '',
        type: []
    }


    companyDetails = async(token) => {
        console.log(token)
        await axios.get(`http://localhost:7000/company`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            let data = response.data.data
            // this.setState({'name': data.name})
            // this.setState({'description': data.description})
            // this.setState({'email': data.email})
            // this.setState({'address': data.address})
            // this.setState({'type': data.type})
            this.setState({companyDetails: data})
            console.log(this.state.companyDetails)
        })
    }


    async componentDidMount() {
        let token = await localStorage.getItem('token')
        await this.companyDetails(token)
    }

    // edit = () => {
    //     console.log(this.state)
    // }

    edit = async() => {
        let token = await localStorage.getItem('token')
        let input = this.state

        axios.post(`http://localhost:7000/company/add`, {
            name: input.name != '' ? input.name : input.companyDetails.name,
            description: input.description != '' ? input.description : input.companyDetails.description,
            email: input.email != '' ? input.email : input.companyDetails.email,
            address: input.address != '' ? input.address : input.companyDetails.address,
            type: input.type != '' ? input.type : input.companyDetails.type,
        }, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            alert("company updated successfully")
            this.props.history.push('/jobs')
        }).catch(err => alert(err))
    }

  
    render() {
        let token = localStorage.getItem('token')
        let companyDetails = this.state.companyDetails 
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Update Company Details</h1>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Email'
                autoComplete
                onChange={(e) => this.setState({ email: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Name'
                autoComplete
                onChange={(e) => this.setState({ name: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Description'
                autoComplete
                onChange={(e) => this.setState({ description: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Address'
                autoComplete
                onChange={(e) => this.setState({ address: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Type'
                autoComplete
                onChange={(e) => this.setState({ type: e.target.value})}
                />
            </Grid>
            <Grid item>
            <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                    marignBotton: '40px',
                    backgroundColor:'#4caf50'
                }}
                variant='outlined'
                color='secondary'
                onClick = {() => this.edit()}
                >Edit</Button>
            </Grid>
            </div>

        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(EditCompany);
// export default withStyles(useStyles, { withTheme: true })(Login);