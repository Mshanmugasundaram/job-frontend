import React, { Component } from 'react';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
import LocationCityIcon from '@material-ui/icons/LocationCity';
import DescriptionIcon from '@material-ui/icons/Description';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople'
import WorkIcon from '@material-ui/icons/Work';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import WcIcon from '@material-ui/icons/Wc';


const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})


class ProfileDetails extends Component {
    state = {
        profileDetails: []
    }

    details = async() => {
        let token = await localStorage.getItem('token')
        await axios.get(`http://localhost:7000/profile`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            let data = response.data.data
            this.setState({profileDetails: data})
            console.log(data)
        }).catch(err => alert(err))
    }

    edit = () => {
        this.props.history.push('/edit/profile')
    }

    goBack = () => {
        this.props.history.push('/jobs')
    }

    async componentWillMount(){
        let token = await localStorage.getItem('token')
        await this.details()
    }
    render () {
        const profile = this.state.profileDetails

        console.log(profile)
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                textAlign: 'center',
                // marginTop: 'px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Profile Details</h1>
            </div>
            <Grid item>
                <div style={{margin: '20px 80px', backgroundColor: '#f1f1f1', padding: "10px 15px", borderRadius: '10px', justifyContent:'center'}}>
                    <PermIdentityIcon style={{display: 'inline'}}/>
                    <h4 style={{ marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Name - </b>{profile.firstName} {profile.lastName}</h4> <br></br>
                    <DescriptionIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Age - </b> {profile.age}</h4><br/>
                    <WcIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Gender - </b> {profile.gender}</h4><br/>
                    <EmojiPeopleIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}> <b>Qualification - </b> {profile.qualification}</h4><br/>
                    {/* <AttachMoneyIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, display: 'inline-block'}}> <b>Salary - </b> {job.salary}</h4><br/> */}
                    <AccessibilityIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize', display: 'inline-block'}}><b>Skills - </b> {profile.skill != undefined ? profile.skill.toString() : ''}</h4><br/>
                    <AccountCircleIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, display: 'inline-block'}}> <b>Experience - </b> {profile.experience}</h4><br/>
                    <LocationOnIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, textTransform: 'capitalize',display: 'inline-block'}}> <b>Location - </b> {profile.location}</h4><br/>
                    {/* <LocationCityIcon style={{display: 'inline'}}/>
                    <h4 style={{marginLeft: '10px', fontFamily:'sans-serif', fontWeight: 400, display: 'inline-block'}}> <b>Industry - </b> {job.industry}</h4><br/> */}

                    <Button style={{backgroundColor:'#f1f1f1', color: '#4caf50', border: 'solid .5px', marginBottom: '10px'}} fullWidth onClick={() => this.edit()}>Edit</Button>
                     <Button style={{backgroundColor:'#f1f1f1', color: '#4caf50', border: 'solid .5px'}} fullWidth onClick={() => this.goBack()}>Go Back</Button>
                </div>
            </Grid>
            </Grid>
            </ThemeProvider>
        )
    }
}


export default withRouter(ProfileDetails)