import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import friends from '../../friends.jpg'
import {withRouter, Redirect, Route, NavLink, Router } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'
import green from '@material-ui/core/colors/green'
// import white from '@material-ui/core/colors/white'
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {Multiselect} from 'multiselect-react-dropdown'
// import Select from 'react-select'
import Chip from '@material-ui/core/Chip';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: green[500],
        }, 
        secondary:{
            main: '#ffffff'
        }
    }
})

const options = [
]
let skills = []
const skillsList = []
class EditProfile extends Component {
    state = {
        skills: [],
        selectedSkills: '',
        skillSet: []
    }

    handleChange = (e) => {
        console.log(e.target.value)
        this.setState({gender: e.target.value})
        console.log(this.state)
    }

    details = async() => {
        let token = await localStorage.getItem('token')
        await axios.get(`http://localhost:7000/profile`, {
            headers: {
                Authorization: `JWT ${token}`
            }
        }).then(response => {
            let data = response.data.data
            this.setState({profileDetails: data})
            console.log(data)
        }).catch(err => alert(err))
    }

    listSkills = (token) => {
        axios.get(`http://localhost:7000/skill/list`)
            .then(response => {
                let data = response.data.data
                this.setState({skills: data})
                console.log(this.state.skills)
                {this.state.skills.map(skill => {
                    console.log(skill.name)
                })}
            }).catch(err => alert(err))
    }

    async componentWillMount() {
        let token = localStorage.getItem('token')
        await this.listSkills(token)
        await this.details()
    }

    skillHandleChange = (e) => {
        console.log(e.target.value)
        this.setState({skillSet: e.target.value})
      }

    editProfile = () => {
        console.log(this.state.skillSet)
        let userId = localStorage.getItem('id')
        let input = this.state
        // console.log(input)
        // console.log({firstName: input.firstName != undefined ? input.firstName : input.profileDetails.firstName,
        //     lastName: input.lastName!= undefined ? input.lastName : input.profileDetails.lastName,
        //     age: input.age!= undefined ? input.age : input.profileDetails.age,
        //     gender: input.gender!= undefined ? input.gender : input.profileDetails.gender,
        //     experience: input.experience!= undefined ? input.experience : input.profileDetails.experience,
        //     skill: input.skillSet!= undefined ? input.skillSet : input.profileDetails.skill,
        //     location: input.location!= undefined ? input.location : input.profileDetails.location,
        //     qualification: input.qualification!= undefined ? input.qualification : input.profileDetails.qualification,
        //     userId: userId})
        //     return
        axios.post(`http://localhost:7000/profile/create`,
            {
                firstName: input.firstName != undefined ? input.firstName : input.profileDetails.firstName,
                lastName: input.lastName!= undefined ? input.lastName : input.profileDetails.lastName,
                age: input.age!= undefined ? input.age : input.profileDetails.age,
                gender: input.gender!= undefined ? input.gender : input.profileDetails.gender,
                experience: input.experience!= undefined ? input.experience : input.profileDetails.experience,
                skill: input.skillSet!= undefined ? input.skillSet : input.profileDetails.skill,
                location: input.location!= undefined ? input.location : input.profileDetails.location,
                qualification: input.qualification!= undefined ? input.qualification : input.profileDetails.qualification,
                userId: userId
        }).then(response => {
            console.log(response)
            // localStorage.setItem('profile', true)
            alert("Profile updated successfully")
            this.props.history.push('/jobs')

        }).catch(err => alert(err))
    }

    // skillHandleChange = selectedSkills => {
    //     this.setState({selectedSkills: selectedSkills.value})
    //     console.log('selected SKills:', selectedSkills)
    //     console.log(this.state.selectedSkills)
    // }

  
    render() {
        let skills = this.state.skills
        let token = localStorage.getItem('token')
        console.log(skills)
        return (
            <ThemeProvider theme={theme}>
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: '#4caf50',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Edit Profile</h1>
                    
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='First Name'
                autoComplete
                onChange={(e) => this.setState({ firstName: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Last Name'
                autoComplete
                onChange={(e) => this.setState({ lastName: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Age'
                autoComplete
                onChange={(e) => this.setState({ age: e.target.value})}
                />
            </Grid>
            <Grid item>
            <FormControl variant="outlined" style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
            <InputLabel htmlFor="outlined-age-native-simple">Gender</InputLabel>
            <Select
            native
            value={this.state.gender}
            onChange={(e) => this.handleChange(e)}
            label="Gender"
                >
                <option aria-label="None" value="" />
                <option value={'male'}>Male</option>
                <option value={'female'}>Female</option>
                <option value={'other'}>Other</option>
                </Select>
            </FormControl>
            </Grid>
            <Grid item>
                {/* <Multiselect style={{width: '100px'}} options={this.state.skills} displayValue="name" /> */}
                <FormControl variant='outlined' style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
                    <InputLabel id="demo-simple-select-label">Select Skill</InputLabel>
                    <Select
                        // labelId="demo-simple-select-label"
                        // id="demo-simple-select"
                        value={this.state.skillSet}
                        variant='outlined'
                        onChange={(e) => this.skillHandleChange(e)}
                        multiple
                        renderValue={(selected) => (
                            selected.join(', ')
                            )}
                    >
                        { this.state.skills.map((skill) => {
                            return (<MenuItem value={skill.name}>
                                <Checkbox checked={this.state.skillSet.indexOf(skill.name) > -1} />
                                <ListItemText primary={skill.name} />
                            </MenuItem>)
                        })
                    }
                </Select>
                </FormControl>
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Location'
                autoComplete
                onChange={(e) => this.setState({ location: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Qualification'
                autoComplete
                onChange={(e) => this.setState({ qualification: e.target.value})}
                />
            </Grid>
            <Grid item>
            <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                color='primary'
                variant='outlined'
                label='Experience'
                autoComplete
                onChange={(e) => this.setState({ experience: e.target.value})}
                />
            </Grid>
            <Grid>
            <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                    marignBotton: '40px',
                    backgroundColor:'#4caf50'
                }}
                variant='outlined'
                color='secondary'
                onClick = {() => this.editProfile()}
                >Edit</Button>
            </Grid>
            </div>

        </Grid>
        </ThemeProvider>
        )
    }
}

export default withRouter(EditProfile);
// export default withStyles(useStyles, { withTheme: true })(Login);