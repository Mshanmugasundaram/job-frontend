import React from 'react';
import { AppBar, Toolbar, Typography, Button, Grid} from '@material-ui/core';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import green from '@material-ui/core/colors/green'
import {Route, withRouter, Router} from 'react-router-dom';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import MenuTwoToneIcon from '@material-ui/icons/MenuTwoTone';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import history from '../../history'

const useStyles = makeStyles(() => ({
    typographyStyles: {
        fontSize: '24px',
        marginLeft: '20px',
        letterSpacing: '1.8px',
        textTransform: 'uppercase'
    },
    appBar: {
        color: 'white',
    }
}))

const theme = createMuiTheme({
    palette: {
        primary: green
    }
})

const Header = () => {
    const classes = useStyles();

    const logout = () => {
        localStorage.clear();
        // console.log(this.props )
    }
    return (
        <Grid>
            <ThemeProvider theme = {theme}>
                <AppBar position = 'static' className={classes.appBar}>
                    <Toolbar>
                        <WorkOutlineIcon/>
                        <Typography className={classes.typographyStyles}>Job Finder</Typography>
                        {localStorage.getItem('token') ? 
                        <ExitToAppIcon style={{marginLeft: '1240px', cursor: 'pointer'}} onClick={() => logout()} /> : ''}
                    </Toolbar>
                </AppBar>
            </ThemeProvider>
        </Grid>
    )
}

export default withRouter(Header);