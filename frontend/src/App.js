import React from 'react';
import './App.css';
import Header from './components/Header/header'
function App() {
  return (
    <div className="App">
      <Header/>
      <h1>REACT PROJECT</h1>
    </div>
  );
}

export default App;
