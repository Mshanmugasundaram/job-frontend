import React, { Component } from 'react';
import {Route, withRouter, Router} from 'react-router-dom';
import Login from '../components/Login/login'
import Signup from '../components/Login/signup'
import ListJobs from '../components/Job/listJobs'
import ViewJob from '../components/Job/viewJob'
import ApplyJob from '../components/Job/applyJob'
import AppliedJobs from '../components/Job/appliedJobs'
import CreateProfile from '../components/Profile/createProfile'
import PostJobs from '../components/Job/postJobs'
import PostedJobs from '../components/Job/postedJobs'
import EditJob from '../components/Job/editJob'
import AddCompany from '../components/Company/addCompany'
import CompanyDetails from '../components/Company/companyDetails'
import EditCompany from '../components/Company/editCompany'
import ProfileDetails from '../components/Profile/profileDetails'
import EditProfile from '../components/Profile/editProfile'
import AddRecruiter from '../components/Recruiter/addRecruiter'
import RequestRecruiter from '../components/Recruiter/requestRecruiter'
import ListRecruiter from '../components/Recruiter/listRecruiter'

class Routes extends Component {
    render() {
        return (
            <Router history={this.props.history}>
                <Route exact path='/' exact component={Login} />
                <Route exact path='/signup' exact component={Signup} />
                <Route exact path='/jobs' exact component={ListJobs} />
                <Route exact path='/view/job' exact component={ViewJob} />
                <Route exact path='/apply' exact component={ApplyJob} />
                <Route exact path='/applied' exact component={AppliedJobs} />
                <Route exact path='/add/profile' exact component={CreateProfile} />
                <Route exact path='/post/job' exact component={PostJobs} />
                <Route exact path='/posted/jobs' exact component={PostedJobs} />
                <Route exact path='/edit/job' exact component={EditJob} />
                <Route exact path='/add/company' exact component={AddCompany} />
                <Route exact path='/company' exact component={CompanyDetails} />
                <Route exact path='/edit/company' exact component={EditCompany} />
                <Route exact path='/profile' exact component={ProfileDetails} />
                <Route exact path='/edit/profile' exact component={EditProfile} />
                <Route exact path='/add/recruiter' exact component={AddRecruiter} />
                <Route exact path='/request' exact component={RequestRecruiter} />
                <Route exact path='/list' exact component={ListRecruiter} />
            </Router>
        )
    }
}

export default withRouter(Routes)